from typing import List, Tuple
import numpy as np
from numpy.typing import NDArray


class SpringNetwork:
    """
    Emulates the evolution over time of a network of weights connected by springs
    """

    def __init__(
        self,
        x_positions: NDArray,
        y_positions: NDArray,
        edge_lengths: NDArray | int | float | None = None,
        spring_constants: NDArray | int | float | None = None,
        masses: NDArray | int | float | None = None,
        x_velocities: NDArray | int | float | None = None,
        y_velocities: NDArray | int | float | None = None,
        damping: NDArray | int | float | None = None,
    ):
        # Check if the lengths are equal then put in object.
        # A few extra steps are necessary for optional arguments.
        N = len(x_positions)
        # assert len(y_positions)==N, "x and y not the same size"
        self._N = N
        self._x_positions = x_positions.copy()
        self._y_positions = y_positions.copy()

        # Here we add optional parameters. It's a bit of a pain as you can see, but it works
        if edge_lengths is not None:
            edge_lengths = edge_lengths * np.ones((N, N), dtype=float)
            assert edge_lengths.shape == (N, N), "edge_lengths not the right size"
            assert all(
                edge_lengths[i, i] == 0 for i in range(N)
            ), "nonzero edge_lenght with self"
            assert all(
                edge_lengths[i, j] == edge_lengths[j, i]
                for i in range(1, N)
                for j in range(i, N)
            ), "nonttriangular edge lenghth matrix"
            self._edge_lengths = edge_lengths.copy()
        else:
            self._edge_lengths = np.ones((N, N), dtype=float)
            for i in range(N):
                self._edge_lengths[i, i] = 0.0

        if spring_constants is not None:
            spring_constants = spring_constants * np.ones((N, N), dtype=float)
            assert spring_constants.shape == (
                N,
                N,
            ), "spring_constant not the right size"
            assert all(
                spring_constants[i, i] == 0 for i in range(N)
            ), "nonzero spring_constant with self"
            assert all(
                spring_constants[i, j] == spring_constants[j, i]
                for i in range(1, N)
                for j in range(i, N)
            ), "nonttriangular spring constant matrix"
            self._spring_constants = spring_constants.copy()
        else:
            self._spring_constants = np.ones((N, N), dtype=float)
            for i in range(N):
                self._spring_constants[i, i] = 0

        if masses is not None:
            masses = np.array(masses, dtype=float)
            assert masses.size in [1, N], "masses is not the right lenght"
            assert np.all(masses > 0), "some masses are either <= 0"
            self._masses = masses.copy()
        else:
            self._masses = np.ones(N, dtype=float)

        if x_velocities is not None:
            x_velocities = x_velocities * np.ones(N, dtype=float)
            assert len(x_velocities) == N, "x_velocities not the right length"
            self._x_velocities = x_velocities.copy()
        else:
            self._x_velocities = np.zeros(N, dtype=float)

        if y_velocities is not None:
            y_velocities = np.ones(N, dtype=float) * y_velocities
            assert len(y_velocities) == N, "y_velocities not the right lenght"
            self._y_velocities = y_velocities.copy()
        else:
            self._y_velocities = np.zeros(N, dtype=float)

        if damping is not None:
            damping = np.array(damping)
            assert damping.size in [N, 1], "Damping vector not the right length."
            self._damping = damping.astype(float)
        else:
            self._damping = np.zeros(1).astype(float)

    def _take_step(self, dt: float = 0.001) -> None:
        """
        Update the position of each vertex.
        To do:
            - Create an animation method
            - Add an optional damping factor (per vertex)
        """
        N = self._N  # for easy access
        # Calculate the distance in each individual direction and absolute
        x_distances = self._x_positions.reshape(1, N) - self._x_positions.reshape(N, 1)
        y_distances = self._y_positions.reshape(1, N) - self._y_positions.reshape(N, 1)

        abs_distances = np.sqrt(x_distances**2 + y_distances**2)

        # Calculate forces from and to each of the nodes
        x_forces = (
            (1 - self._edge_lengths / abs_distances)
            * self._spring_constants
            * x_distances
        )
        y_forces = (
            (1 - self._edge_lengths / abs_distances)
            * self._spring_constants
            * y_distances
        )

        # Fix divide by zero errors by setting to zero.
        # This can cause problems if two different vertices overlap.
        # Maybe there is a better solution
        for i in range(N):
            x_forces[i, i] = y_forces[i, i] = 0.0

        # Add damping
        x_forces -= self._x_velocities * self._damping
        y_forces -= self._y_velocities * self._damping

        # Accumulate the forces from all the differeng directions
        x_accelleration = np.sum(x_forces, axis=1) / self._masses
        y_accelleration = np.sum(y_forces, axis=1) / self._masses

        # Update the velocity
        self._x_velocities += dt * x_accelleration
        self._y_velocities += dt * y_accelleration

        # Update the position
        self._x_positions += dt * self._x_velocities
        self._y_positions += dt * self._y_velocities

    def simulate_n_seconds(self, T: float, dt=0.01, **kwargs):
        return self.simulate_many_steps(int(T / dt), dt, **kwargs)

    def simulate_many_steps(self, no_steps: int, dt: float, last_step=False) -> NDArray:
        N = self._N
        out = np.empty((N, no_steps, 2), dtype=float)
        out[:, 0, 0] = self._x_positions[:]
        out[:, 0, 1] = self._y_positions[:]
        for i in range(1, no_steps):
            self._take_step(dt)
            out[:, i, 0] = self._x_positions[:]
            out[:, i, 1] = self._y_positions[:]
        return out

    # Some access to properties of the thing
    @property
    def N(self) -> int:
        return self._N

    @property
    def positions(self) -> List[Tuple[float, float]]:
        return list(zip(self._x_positions, self._y_positions))

    @property
    def damping(self):
        return self._damping

    @damping.setter
    def damping(self, value):
        value = np.array(value, dtype=float)
        assert value.size in [1, self._N], "not the right size"
        self._damping = value


# returns a matrix that is symmetric in the diagonal
# throws away all values in the lower triangular
def triangular_mirror(a: NDArray) -> None:
    assert a.ndim == 2, "not 2D"
    assert len(set(a.shape)) == 1, "not square"

    for i in range(len(a)):
        for j in range(i, len(a)):
            a[j, i] = a[i, j]


# Set all values in the diagonal to zero (in place)
def remove_diagonal(a: NDArray) -> None:
    assert a.ndim == 2, "not 2D"
    assert len(set(a.shape)) == 1, "not square"
    for i in range(len(a)):
        a[i, i] = 0


def sparsify(a: NDArray, sparseness: float) -> None:
    if sparseness >= 1.0:
        return
    else:
        for index in np.ndindex(a.shape):
            if np.random.random() > sparseness:
                a[index] = 0.0


def random_network(
    N: int,
    group_velocity: Tuple[float, float] = (0, 0),
    edge_chance: float = 1.0,
    speed_multiplier: float = 0.0,
    mass_multiplier: float = 1.0,
    spring_multiplier: float = 1.0,
) -> SpringNetwork:
    spring_constants = spring_multiplier * np.random.random((N, N))
    sparsify(spring_constants, edge_chance) # Remove some edges completely
    triangular_mirror(spring_constants)     # Make sure that the matrix is triangular symmetric
    remove_diagonal(spring_constants)       # Set all loops to zero

    masses = mass_multiplier * np.random.random(N) # Everybody gets a mass

    # We make sure that the group velocity is zero
    x_velocities = speed_multiplier * (
        (tmp := np.random.random(N)) - np.average(tmp * masses) / np.average(masses)
    )
    y_velocities = speed_multiplier * (
        (tmp := np.random.random(N)) - np.average(tmp * masses) / np.average(masses)
    )
    x_velocities += group_velocity[0]
    y_velocities += group_velocity[1]
    network = SpringNetwork(
        2*np.random.random(N)-1,
        2*np.random.random(N)-1,
        x_velocities=x_velocities,
        y_velocities=y_velocities,
        masses=masses,
        spring_constants=spring_constants,
    )

    return network


def circle_network(
    N: int, r=0.5, v: float = 1.0, group_velocity: tuple[float, float] = (0.0, 0.0)
):
    angles = np.linspace(0, 2 * np.pi, N, endpoint=False)
    p_x = np.cos(angles) * r
    p_y = np.sin(angles) * r
    v_x = -np.sin(angles) * v + group_velocity[0]
    v_y = np.cos(angles) * v + group_velocity[1]
    network = SpringNetwork(p_x, p_y, x_velocities=v_x, y_velocities=v_y)
    return network


if __name__ == "__main__":
    # Test code:
    pass
