from math import sqrt
from manim import Dot, Graph, Scene, random_color
from springnetwork import random_network
from manimxyzmove import MoveAlongXYZPath
import numpy as np


class SpringNetworkAnimation(Scene):
    def construct(self):
        # Parameters
        N     = 4
        T     = 120
        dt    = 0.001
        multi = 3

        # Create simulation object
        network = random_network(
            N, edge_chance=0.7, speed_multiplier=2.0, spring_multiplier=7
        )

        # Create visual
        vertex_mobjects = {i:Dot(radius=0.4*sqrt(m)) for i,m in enumerate(network._masses)}
        visual_network = Graph(
            vertices        = [*range(N)],
            edges           = [(i, j) for i in range(N) for j in range(i + 1, N) if network._spring_constants[i,j]!=0],
            vertex_mobjects = vertex_mobjects,
        )
        for p, v in zip(network.positions, visual_network.vertices.values()):
            v.move_to([*p, 0])
            v.color = random_color()
        for e in visual_network.edges.values():
            e.color = random_color()

        # Simulate and extract results
        raw_result = network.simulate_n_seconds(T, dt)
        ps = multi * np.concatenate(
            [raw_result,
             np.zeros((*raw_result.shape[:-1],1,),dtype=float,)],
            axis=2,
        )
        ts = np.linspace(0, T, ps.shape[1])

        # Dimensions are hard, okay?
        assert ps.shape == (N, int(T / dt), 3), "Something wrong"

        self.add(visual_network)
        self.play(
            *(
                MoveAlongXYZPath(visual_network.vertices[i], ts, ps[i], is_sorted=True)
                for i in range(N)
            )
        )
        self.wait()
